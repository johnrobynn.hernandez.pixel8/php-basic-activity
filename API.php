<?php

class API {

  function printFullName($name) {
    echo "Full Name: ".$name;
  }

  function printHobbies($hobbies) {
    echo "\nHobbies:";
    foreach ($hobbies as $hobby) {
      echo "\n\t".$hobby;
    }
  }

  function printPersonalInfo($info) {
    echo "\nAge: ".$info->age;
    echo "\nEmail: ".$info->email;
    echo "\nBirthday: ".$info->birthday;
  }

}

$api = new API();

$name = "John Robynn R. Hernandez";
$api->printFullName($name);

$hobbies = array("Playing Videogames", "Sleeping", "Listening to Music", "Watching Netflix Shows");
$api->printHobbies($hobbies);

//empty class
$info = new stdClass();
$info->age = 22;
$info->email = "johnrobynn.hernandez.pixel8@gmail.com";
$info->birthday = "December 2, 2000";
$api->printPersonalInfo($info);

?>